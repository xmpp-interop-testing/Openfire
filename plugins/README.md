# No Source Here

This is a shim of Openfire being used to experiment and implement the work as part of the [XMPP Interop Testing](https://xmpp-interop-testing.github.io/) project.

See the real version [here](https://github.com/igniterealtime/Openfire).
